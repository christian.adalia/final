import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { HttpClient} from '@angular/common/http';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  
  public prod_name:any;
  public price:any;
  public qty:any;
  order: any =[];
  constructor(public toastCtrl: ToastController,private http: HttpClient) { }

  ngOnInit() {

    this.http.get('http://localhost:3000/orders')
    .subscribe(data=>{
      console.log(data);
      this.order = data;
      
    })

    this.prod_name = window.localStorage.getItem('Product_Name');
    this.price = window.localStorage.getItem('Price');
    this.qty = window.localStorage.getItem('qty');
   
  }
   
}
