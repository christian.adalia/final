import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { AlertController } from '@ionic/angular';
import { HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

//  fullname:string;
//  username:string;
//  password:string;
//  user: any=[];

 public signIn_user={
  fname: "",
  uname:"",
  pword:""
}

  constructor(private router: Router, public alertController: AlertController, private http: HttpClient) { }
 
  
  register(){

    localStorage.setItem('FullName', this.signIn_user.fname)
    localStorage.setItem('username', this.signIn_user.uname)
    localStorage.setItem('password', this.signIn_user.pword)
    

  }
  
  ngOnInit() {
   
  }
  
  navigate(){
    this.router.navigate(['/home'])
    
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Message',
     
      message: 'You have Successfully Registered' +" " +this.signIn_user.fname,
      buttons: ['OK']
    });
  
    await alert.present();
  
    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  
}
