import { Component, OnInit } from '@angular/core';
import {  NavController } from '@ionic/angular';
import { HttpClient} from '@angular/common/http';
@Component({
  selector: 'app-adminpage',
  templateUrl: './adminpage.page.html',
  styleUrls: ['./adminpage.page.scss'],
})
export class AdminpagePage implements OnInit {
  user: any =[];

  constructor(public navCtrl: NavController,private http: HttpClient) {
   
  //   this.username = [
  //     'Christian',
  //     'Adrian',
  //     'Gerald',
  //     'Cocosa',
  //     'Nicole',
  //     'Jasmin',
  //     'Rachel',
  //     'Erica',
  //     'Rizalene',
  //     'Raffy',
      
  // ];

   }

  ngOnInit() {
    this.http.get('http://localhost:3000/')
    .subscribe(data=>{
      console.log(data);
      this.user = data;
      
    })
  }

}
