import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {
  order: any =[];
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get('http://localhost:3000/orders')
    .subscribe(data=>{
      console.log(data);
      this.order = data;
      
    })

   
  }

}
