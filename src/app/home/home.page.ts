import { Component } from '@angular/core';
import { NavigationEnd } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { RegisterPage } from '../register/register.page';
import {Router} from '@angular/router';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  username:string="";
  password:string="";

  public form={
    username:"",
    password:"",
  }

  constructor(public navCtrl: NavController, public toastCtrl: ToastController,private router: Router, public alertController: AlertController) {
    
  }
  
 async signin(){
// if(this.username==""){
//   const toast = await this.toastCtrl.create({
//     message: 'Username cannot be empty',
//     duration:3000
//   });
//   return await toast.present();
// }else if(this.password==""){
//   const toast = await this.toastCtrl.create({
//     message: 'Password cannot be empty',
//     duration:3000
//   });
//   return await toast.present();
// }
// else if (this.username=="admin" && this.password=="admin"){
//   this.router.navigate(['/menu'])
//   const toast = await this.toastCtrl.create({
//     message: 'Welcome'+ " "+this.username,
//     duration:3000
//   });
//   return await toast.present();
// }
// else if (this.username!="admin" && this.password!="admin"){
  
//   const toast = await this.toastCtrl.create({
//     message: 'Invalid username or password',
//     duration:3000
//   });
//   return await toast.present();
// }

if(localStorage.getItem('username') == this.form.username && localStorage.getItem('password') == this.form.password){
  this.router.navigate(['/menu'])
  const toast = await this.toastCtrl.create({
    message: 'Welcome' +" " +this.form.username,
    duration:3000
  });
  return await toast.present();
}
else{
  const toast = await this.toastCtrl.create({
        message: 'Invalid username or password',
        duration:3000
      });
      return await toast.present();
}
if(this.form.username === "" || this.form.password === ""){
  const toast = await this.toastCtrl.create({
        message: 'Username cannot be empty',
        duration:3000
      });
      return await toast.present();
}

  }

navigate(){
  this.router.navigate(['/register'])
}
clear(){
  this.username="";
  this.password="";
}

async presentAlert() {
  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    header: 'Message',
   
    message: 'Welcome! '+this.form.username,
    buttons: ['OK']
  });

  await alert.present();

  const { role } = await alert.onDidDismiss();
  console.log('onDidDismiss resolved with role', role);
}
async adminside(){
  if(this.username==""){
    const toast = await this.toastCtrl.create({
      message: 'Username cannot be empty',
      duration:3000
    });
    return await toast.present();
  }else if(this.password==""){
    const toast = await this.toastCtrl.create({
      message: 'Password cannot be empty',
      duration:3000
    });
    return await toast.present();
  }
  else if (this.username=="admin" && this.password=="admin"){
    this.router.navigate(['/adminpage'])
    const toast = await this.toastCtrl.create({
      message: 'Welcome'+ " "+this.username,
      duration:3000
    });
    return await toast.present();
  }
  else if (this.username!="admin" && this.password!="admin"){
    
    const toast = await this.toastCtrl.create({
      message: 'Invalid username or password',
      duration:3000
    });
    return await toast.present();
  }
  
}
}
