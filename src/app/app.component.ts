import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  public username:any;
  
  constructor() {
  
  }
ngOnInit(){
  this.username = window.localStorage.getItem('username');
}
}
