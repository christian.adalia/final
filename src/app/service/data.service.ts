import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

export class Post {
  product_name:string;
  price:string;
  quantity:string;
}

@Injectable({
  providedIn: 'root'
})
export class DataService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient) { }
  

  createPost(user: Post): Observable<any> {
    return this.http.post<Post>('http://localhost:3000/order', user, this.httpOptions)
      .pipe(
        catchError(this.handleError<Post>('Error occured'))
      );
  }


 

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };

  
}
}
