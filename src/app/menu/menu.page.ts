
import { AlertController } from '@ionic/angular';
import { Component, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DataService } from '../service/data.service';
import { FormGroup, FormBuilder } from "@angular/forms";
@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {


  

  constructor(public alertController: AlertController, private router: Router, private zone: NgZone, public formBuilder: FormBuilder,private dataService: DataService) {

   }
   chocolate(){

    localStorage.setItem('Product_Name', 'chocolate')
    localStorage.setItem('Price', '150')
    localStorage.setItem('isValidate', 'false')
  }
  strawberry(){

    localStorage.setItem('Product_Name', 'strawberry')
    localStorage.setItem('Price', '200')
    localStorage.setItem('isValidate', 'false')
    

  }
  pineapple(){

    localStorage.setItem('Product_Name', 'pineapple')
    localStorage.setItem('Price', '160')
    localStorage.setItem('isValidate', 'false')
    

  }
  milkmelon(){

    localStorage.setItem('Product_Name', 'milk-melon')
    localStorage.setItem('Price', '112')
    localStorage.setItem('isValidate', 'false')
    

  }
  avocado(){

    localStorage.setItem('Product_Name', 'avocado strawberry')
    localStorage.setItem('Price', '180')
    localStorage.setItem('isValidate', 'false')
    

  }

  option={
    slidesPerView: 1.5,
    centeredSlides: true,
    loop:true,
    spaceBetween: 10,
    // autoplay: true,
  }
  ngOnInit() {
  }
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmation Message!',
      message: 'Added to cart',
      buttons: [
         {
          text: 'Okay',
          handler: () => {
            console.log('Item added to cart');
          }
        }
      ]
    });

    await alert.present();
  }
}
