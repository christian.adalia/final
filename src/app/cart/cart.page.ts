  
import { Component, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { DataService } from '../service/data.service';
import { FormGroup, FormBuilder } from "@angular/forms";
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  public prod_name:any;
  public price:any;
  public isValidate:any;
  public qty:any;


  userForm: FormGroup;

  constructor(public alertController: AlertController, private router: Router, public toastCtrl: ToastController, private zone: NgZone, public formBuilder: FormBuilder,private dataService: DataService) { 
    this.userForm =  this.formBuilder.group({
    product_name: [localStorage.getItem('Product_Name')],
      price:[localStorage.getItem('Price')],
      quantity:[localStorage.getItem('Quantity')]
     
     
    })

    
  }

  postOrder(){
    
    if (!this.userForm.valid ) {
     return false;
   } else {
     this.dataService.createPost(this.userForm.value)
       .subscribe((response) => {
         this.zone.run(() => {
           this.userForm.reset();
          //  this.router.navigate(['/menu']);
         })
       });
   }
  }

 
  ngOnInit() {

    

    this.prod_name = window.localStorage.getItem('Product_Name');
    this.price = window.localStorage.getItem('Price');
    this.isValidate = window.localStorage.getItem('isValidate');
    localStorage.setItem('isValidate', 'true')
  }
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmation Message!',
      message: 'Checkout !',
      buttons: [
         {
          text: 'Okay',
          handler: () => {
            console.log('Item has been checkout');
          }
        }
      ]
    });

    await alert.present();
  }
  async checkout(){
    if(this.prod_name=="" && this.price==""){
      const toast = await this.toastCtrl.create({
        message: 'Please select item ',
        duration:3000
      });
      return await toast.present();
    }else{
    localStorage.setItem('isValidate', 'true')
    this.router.navigate(['/profile'])
    this.qty = window.localStorage.getItem('qty');
    this.prod_name = "";
    this.price = "";
  }
}
clear(){
      this.prod_name = "";
      this.price = "";
      localStorage.setItem('Product_Name', "");
      localStorage.setItem('Price', "");
}


  quantity:number=1;
  i=1
  plus(){
      if(this.i !=5){
        this.i++;
        this.quantity = this.i;
       
   
      }
  }
  minus(){
    if(this.i !=1){
      this.i--;
      this.quantity = this.i;
    }
  }
}
